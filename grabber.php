<?php
/**
 * Created by PhpStorm.
 * User: ares7
 * Date: 24.10.2016
 * Time: 12:44
 */

namespace Grabber;


function getElementsByClassName($dom, $classname)
{
    $dxp = new \DOMXPath($dom);
    return $dxp->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");
}

class Champions {
    public $error = "";
    public $_self_link;

    public function __construct($player) {

        $this->_self_link = "http://paladins.guru/profile/pc/" . $player . "/champions";

        $content = file_get_contents($this->_self_link);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);


        if (getElementsByClassName($dom, "profile-header")->length == 0)
            $this->error = "Player not found";

        $champda = new ChampDa();
    }
}

class Champion {
    public $name;

    public $kills, $deaths, $assists;

    public $wins, $taken, $gold, $inhand, $mitigated, $player, $played;


    public function __construct($doc, $champda) {

    }
}

class ChampDa {
    public $stat_index = 0;
    public $champ_index = 0;
}



class Profile {
    public $error = "";


    public $username;
    public $level;
    public $playtime;
    public $last_seen;
    public $platform;
    public $mastery;

    // Global WL
    public $global_wins, $global_losses, $global_wl_rank, $global_wl_top, $global_wl;

    // Global KDA
    public $global_kills, $global_deaths, $global_assists, $global_kda_rank, $global_kda_top, $global_kda;

    // Flank Stats
    public $flank_playtime, $flank_played, $flank_wins, $flank_losses, $flank_wl, $flank_kda, $flank_kills,
        $flank_deaths, $flank_assists, $flank_gpm;

    // Front Line Stats
    public $frontline_playtime, $frontline_played, $frontline_wins, $frontline_losses, $frontline_wl, $frontline_kda, $frontline_kills,
        $frontline_deaths, $frontline_assists, $frontline_gpm;

    // Damage Stats
    public $damage_playtime, $damage_played, $damage_wins, $damage_losses, $damage_wl, $damage_kda, $damage_kills,
        $damage_deaths, $damage_assists, $damage_gpm;

    // Support Stats
    public $support_playtime, $support_played, $support_wins, $support_losses, $support_wl, $support_kda, $support_kills,
        $support_deaths, $support_assists, $support_gpm;

    // Top Champions
    public $top_champ1, $top_champ2, $top_champ3, $top_champ4, $tom_champ5;


    private $_self_link;


    public function __construct($username)
    {
        $this->username = strtolower($username);
        $this->_self_link = "http://paladins.guru/profile/pc/" . $username;

        $content = file_get_contents($this->_self_link);
        $dom = new \DOMDocument();
        $dom->loadHTML($content);


        $this->level = getElementsByClassName($dom, "level")->item(0)->nodeValue;

        if (empty($this->level))
            $this->error = "Player not found";

        $tmp = getElementsByClassName($dom, "profile-header-stat")->item(0)->nodeValue;
        $this->mastery = explode(" ", $tmp);
        $this->mastery = $this->mastery[1];
        $tmp = getElementsByClassName($dom, "profile-header-stat")->item(1)->nodeValue;
        $this->platform = explode(" ", $tmp);
        $this->platform = $this->platform[1];
        $this->playtime = getElementsByClassName($dom, "lg-num")->item(0)->nodeValue;
        $this->last_seen = getElementsByClassName($dom, "lg-num")->item(1)->nodeValue;

        $lgnum = getElementsByClassName($dom, "lg-num")->item(2)->nodeValue;
        $lgnum = str_replace(' ', '', $lgnum);

        $stat = getElementsByClassName($dom, "stat");

        $wlloada = explode("-", $lgnum);

        $this->global_wins = $wlloada[0];
        $wlloadb = explode("(", $wlloada[1]);

        $this->global_losses = $wlloadb[0];
        $this->global_wl = str_replace(')', '', $wlloadb[1]);

        $wlloada = explode(")", $stat->item(2)->nodeValue);
        $wlloadb = explode("-", $wlloada[1]);
        $wlloadc = explode(" ", $wlloadb[0]);
        $wlloadd = explode(" ", $wlloadb[1]);

        $this->global_wl_rank = $wlloadc[4];
        $this->global_wl_top = $wlloadd[2];

        $wlloada = explode(")", $stat->item(3)->nodeValue);
        $wlloadb = explode("-", $wlloada[1]);
        $wlloadc = explode(" ", $wlloadb[0]);
        $wlloadd = explode(" ", $wlloadb[1]);

        $this->global_kda_rank = $wlloadc[4];
        $this->global_kda_top = $wlloadd[2];

        $kda_a = explode("/", getElementsByClassName($dom, "lg-num")->item(3)->nodeValue);

        $this->global_kills = $kda_a[0];
        $this->global_deaths = $kda_a[1];

        $kda_b = explode(' ', $kda_a[2]);

        $this->global_assists = $kda_b[1];

        $kda_b[2] = str_replace('(', '', $kda_b[2]);
        $kda_b[2] = str_replace(')', '', $kda_b[2]);

        $this->global_kda = $kda_b[2];

        $this->flank_playtime = $this->statPart($stat->item(4));
        $this->flank_played = $this->statPart($stat->item(5));
        $this->flank_wins = $this->statPart($stat->item(6));
        $this->flank_losses = $this->statPart($stat->item(7));
        $this->flank_wl = $this->statPart($stat->item(8));
        $this->flank_kda = $this->statPart($stat->item(9));
        $this->flank_kills = $this->statPart($stat->item(10));
        $this->flank_deaths = $this->statPart($stat->item(11));
        $this->flank_assists = $this->statPart($stat->item(12));
        $this->flank_gpm = $this->statPart($stat->item(13));

        $this->frontline_playtime = $this->statPart($stat->item(14));
        $this->frontline_played = $this->statPart($stat->item(15));
        $this->frontline_wins = $this->statPart($stat->item(16));
        $this->frontline_losses = $this->statPart($stat->item(17));
        $this->frontline_wl = $this->statPart($stat->item(18));
        $this->frontline_kda = $this->statPart($stat->item(19));
        $this->frontline_kills = $this->statPart($stat->item(20));
        $this->frontline_deaths = $this->statPart($stat->item(21));
        $this->frontline_assists = $this->statPart($stat->item(22));
        $this->frontline_gpm = $this->statPart($stat->item(23));

        $this->damage_playtime = $this->statPart($stat->item(24));
        $this->damage_played = $this->statPart($stat->item(25));
        $this->damage_wins = $this->statPart($stat->item(26));
        $this->damage_losses = $this->statPart($stat->item(27));
        $this->damage_wl = $this->statPart($stat->item(28));
        $this->damage_kda = $this->statPart($stat->item(29));
        $this->damage_kills = $this->statPart($stat->item(30));
        $this->damage_deaths = $this->statPart($stat->item(31));
        $this->damage_assists = $this->statPart($stat->item(32));
        $this->damage_gpm = $this->statPart($stat->item(33));

        $this->support_playtime = $this->statPart($stat->item(34));
        $this->support_played = $this->statPart($stat->item(35));
        $this->support_wins = $this->statPart($stat->item(36));
        $this->support_losses = $this->statPart($stat->item(37));
        $this->support_wl = $this->statPart($stat->item(38));
        $this->support_kda = $this->statPart($stat->item(39));
        $this->support_kills = $this->statPart($stat->item(40));
        $this->support_deaths = $this->statPart($stat->item(41));
        $this->support_assists = $this->statPart($stat->item(42));
        $this->support_gpm = $this->statPart($stat->item(43));

        $this->top_champ1 = getElementsByClassName($dom, "name")->item(0)->nodeValue;
        $this->top_champ2 = getElementsByClassName($dom, "name")->item(1)->nodeValue;
        $this->top_champ3 = getElementsByClassName($dom, "name")->item(2)->nodeValue;
        $this->top_champ4 = getElementsByClassName($dom, "name")->item(3)->nodeValue;
        $this->top_champ5 = getElementsByClassName($dom, "name")->item(4)->nodeValue;
    }

    private function statPart($p) {
        $e = explode(" ", $p->nodeValue);

        $res = "";
        for($i = 0; $i < count($e) - 2; $i ++) {
            $res .= $e[$i] . " ";
        }
        return $res;
    }


    public function debug()
    {
        echo "<div class='grabberDebug'>";
        echo "<table style='border: 1px solid black;'>";
        echo "<tr><td>Error</td><td> $this->error </td></tr>";
        echo "<tr><td>Username</td><td> $this->username </td></tr>";
        echo "<tr><td>Play Time</td><td> $this->playtime </td></tr>";
        echo "<tr><td>Last Seen</td><td> $this->last_seen </td></tr>";
        echo "<tr><td>Platform</td><td> $this->platform </td></tr>";
        echo "<tr><td>Mastery</td><td> $this->mastery </td></tr>";
        echo "<tr><td>Level</td><td> $this->level </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Global Wins</td><td> $this->global_wins </td></tr>";
        echo "<tr><td>Global Losses</td><td> $this->global_losses </td></tr>";
        echo "<tr><td>Global WL Rank</td><td> $this->global_wl_rank </td></tr>";
        echo "<tr><td>Global WL Top</td><td> $this->global_wl_top </td></tr>";
        echo "<tr><td>Global WL Rate</td><td> $this->global_wl </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Global Kills</td><td> $this->global_kills </td></tr>";
        echo "<tr><td>Global Deaths</td><td> $this->global_deaths </td></tr>";
        echo "<tr><td>Global Assists</td><td> $this->global_assists </td></tr>";
        echo "<tr><td>Global KDA Rank</td><td> $this->global_kda_rank </td></tr>";
        echo "<tr><td>Global KDA Top</td><td> $this->global_kda_top </td></tr>";
        echo "<tr><td>Global KDA Rate</td><td> $this->global_kda </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";
        echo "<tr><td></td><td>  </td></tr>";


        echo "<tr><td>Flank Playtime</td><td> $this->flank_playtime </td></tr>";
        echo "<tr><td>Flank Played</td><td> $this->flank_played </td></tr>";
        echo "<tr><td>Flank Wins</td><td> $this->flank_wins </td></tr>";
        echo "<tr><td>Flank Losses</td><td> $this->flank_losses </td></tr>";
        echo "<tr><td>Flank WL Rate</td><td> $this->flank_wl </td></tr>";
        echo "<tr><td>Flank KDA Rate</td><td> $this->flank_kda </td></tr>";
        echo "<tr><td>Flank Kills</td><td> $this->flank_kills </td></tr>";
        echo "<tr><td>Flank Deaths</td><td> $this->flank_deaths </td></tr>";
        echo "<tr><td>Flank Assists</td><td> $this->flank_assists </td></tr>";
        echo "<tr><td>Flank GPM</td><td> $this->flank_gpm </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Front Line Playtime</td><td> $this->frontline_playtime </td></tr>";
        echo "<tr><td>Front Line Played</td><td> $this->frontline_played </td></tr>";
        echo "<tr><td>Front Line Wins</td><td> $this->frontline_wins </td></tr>";
        echo "<tr><td>Front Line Losses</td><td> $this->frontline_losses </td></tr>";
        echo "<tr><td>Front Line WL Rate</td><td> $this->frontline_wl </td></tr>";
        echo "<tr><td>Front Line KDA Rate</td><td> $this->frontline_kda </td></tr>";
        echo "<tr><td>Front Line Kills</td><td> $this->frontline_kills </td></tr>";
        echo "<tr><td>Front Line Deaths</td><td> $this->frontline_deaths </td></tr>";
        echo "<tr><td>Front Line Assists</td><td> $this->frontline_assists </td></tr>";
        echo "<tr><td>Front Line GPM</td><td> $this->frontline_gpm </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Damage Playtime</td><td> $this->damage_playtime </td></tr>";
        echo "<tr><td>Damage Played</td><td> $this->damage_played </td></tr>";
        echo "<tr><td>Damage Wins</td><td> $this->damage_wins </td></tr>";
        echo "<tr><td>Damage Losses</td><td> $this->damage_losses </td></tr>";
        echo "<tr><td>Damage WL Rate</td><td> $this->damage_wl </td></tr>";
        echo "<tr><td>Damage KDA Rate</td><td> $this->damage_kda </td></tr>";
        echo "<tr><td>Damage Kills</td><td> $this->damage_kills </td></tr>";
        echo "<tr><td>Damage Deaths</td><td> $this->damage_deaths </td></tr>";
        echo "<tr><td>Damage Assists</td><td> $this->damage_assists </td></tr>";
        echo "<tr><td>Damage GPM</td><td> $this->damage_gpm </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Support Playtime</td><td> $this->support_playtime </td></tr>";
        echo "<tr><td>Support Played</td><td> $this->support_played </td></tr>";
        echo "<tr><td>Support Wins</td><td> $this->support_wins </td></tr>";
        echo "<tr><td>Support Losses</td><td> $this->support_losses </td></tr>";
        echo "<tr><td>Support WL Rate</td><td> $this->support_wl </td></tr>";
        echo "<tr><td>Support KDA Rate</td><td> $this->support_kda </td></tr>";
        echo "<tr><td>Support Kills</td><td> $this->support_kills </td></tr>";
        echo "<tr><td>Support Deaths</td><td> $this->support_deaths </td></tr>";
        echo "<tr><td>Support Assists</td><td> $this->support_assists </td></tr>";
        echo "<tr><td>Support GPM</td><td> $this->support_gpm </td></tr>";

        echo "<tr><td></td><td>  </td></tr>";
        echo "<tr><td></td><td>  </td></tr>";

        echo "<tr><td>Top Champion 1</td><td> $this->top_champ1 </td></tr>";
        echo "<tr><td>Top Champion 2</td><td> $this->top_champ2 </td></tr>";
        echo "<tr><td>Top Champion 3</td><td> $this->top_champ3 </td></tr>";
        echo "<tr><td>Top Champion 4</td><td> $this->top_champ4 </td></tr>";
        echo "<tr><td>Top Champion 5</td><td> $this->top_champ5 </td></tr>";

        echo "</table>";
        echo "</div>";
    }
}